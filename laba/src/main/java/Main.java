import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;


import org.junit.Assert;
//import java.util.Scanner;

class Directory {
    static final int DIR_MAX_ELEMS = 20;

    static String path;

    public static void create(String pathToCreate, String name) {
        path = pathToCreate + '\\' + name;
        File dir = new File(pathToCreate);


        File file = new File(path);

        int numberOfSubfolders = 0;
        File listDir[] = dir.listFiles();
        if (listDir != null) {
            for (int i = 0; i < listDir.length; i++) {
                if (listDir[i].isDirectory()) {
                    numberOfSubfolders++;
                }
            }
        }

        //Creating a File object

        //Creating the directory
        ;
        if (!(numberOfSubfolders < DIR_MAX_ELEMS)) {
            System.out.println("Sorry couldn’t create specified directory");
            return;
        }
        if (file.mkdir()) {
            System.out.println("Directory created successfully");
        } else {
            System.out.println("Sorry couldn’t create specified directory");
        }

    }

    public static void delete(String path) {
        File dir = new File(path);
        if (!(dir.exists())) {
            System.out.println("Не існує");
            return;
        }
        if (!(dir.isDirectory())) {
            System.out.println("Не директорія");
            return;
        }
        dir.delete();
        System.out.println("Директорія видалена.");
    }

    public static void content(String path) {
        File dir = new File(path);
        if (dir.exists()) {
            for (int i = 0; i < dir.listFiles().length; i++) {
                System.out.println(dir.listFiles()[i].getName());
            }
            return;
        }

    }

    public static void move(String path, String newPath) throws IOException, ClassNotFoundException {
        if (new File(path).isDirectory()) {
            {
                File dir[] = new File(path).listFiles();
                File newDir = new File(newPath);
                create(newDir.getPath(), new File(path).getName());
                for (int i = 0; i < dir.length; i++) {
                    //newDir = new File

                    move(dir[i].getPath(), newDir.getPath() + '\\' + new File(path).getName());
                    dir[i].delete();
                }
                return;
            }
        }
        if (!TextFile.check(path)) {
            Object data = BinaryFile.read(path);


            BinaryFile.create(newPath + '\\' + new File(path).getName(), data);
            //System.out.println(new File(path).delete());
            BinaryFile.delete(path);
        } else {
            String content = TextFile.read(path);
            TextFile.create(newPath + '\\' + new File(path).getName(), content);
            TextFile.delete(path);
        }
    }


}

class BinaryFile<T> {
    public static void create(String path, Object obj) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(obj);
        fos.close();
        oos.close();

    }

    public static Object read(String path) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object val = ois.readObject();
        fis.close();
        ois.close();

        return val;
    }

    public static void delete(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }

    }

    public static void move(String path, String newPath) throws IOException, ClassNotFoundException {
        File file = new File(path);
        if (!file.exists() || file.isDirectory() || !new File(newPath).isDirectory()) {
            System.out.println("file does not exist");
            return;
        }
        newPath += '\\' + file.getName();
        Object obj = read(path);
        File newFile = new File(newPath);
        newFile.createNewFile();
        create(newPath, obj);
        file.delete();
    }
}

class TextFile {
    public static void create(String path, String info) throws IOException {
        if (!check(path)) {
            System.out.println('n');
            return;
        }
        if (new File(path).exists()) {
        } else {
            boolean b = new File(path).createNewFile();
        }
        FileWriter writer = new FileWriter(path, true);
        PrintWriter printer = new PrintWriter(writer);
        printer.println(info);
        printer.close();
        writer.close();
    }

    public static void move(String path, String newPath) throws IOException, ClassNotFoundException {
        if (!check(path)) {
            System.out.println("error");
            return;
        }
        File file = new File(path);
        if (!file.exists() || file.isDirectory() || !new File(newPath).isDirectory()) {
            System.out.println("error");
            return;
        }
        newPath += '\\' + file.getName();
        String str = read(path);
        File newFile = new File(newPath);
        newFile.createNewFile();
        create(newPath, str);
        boolean b = file.delete();
    }

    public static void append(String path, String str) throws IOException {
        if (!check(path)) {
            System.out.println('n');
            return;
        }
        if (new File(path).exists()) {
        } else {
            System.out.println("No file exists");
        }
        FileWriter writer = new FileWriter(path, true);
        PrintWriter printer = new PrintWriter(writer);
        printer.println(str);
        printer.close();
        writer.close();
    }

    public static boolean check(String txt) {
        if (txt.charAt(txt.length() - 1) == 't') {
            if (txt.charAt(txt.length() - 2) == 'x') {
                if (txt.charAt(txt.length() - 3) == 't') {
                    if (txt.charAt(txt.length() - 4) == '.') {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String read(String path) throws IOException {
        FileReader reader = new FileReader(path);
        BufferedReader bRreader = new BufferedReader(reader);
        String res = "";
        String temp = bRreader.readLine();
        while (temp != null) {
            res += temp + "\n";
            temp = bRreader.readLine();
        }
        bRreader.close();
        reader.close();
        return res;
    }

    public static void delete(String path) {
        if (check(path)) {
            new File(path).delete();
        }
    }
}

class Buffer<T> {
    final int MAX_BUF_FILE_SIZE = 25;
    String path;
    int count = 0;

    public Buffer(String Path) {
        this.path = Path;
        File buffer = new File(path);
        try {
            buffer.createNewFile();
            ArrayList<Object> list = new ArrayList<Object>();
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream((fos));
            oos.writeObject(list);
            oos.close();
            ;
            fos.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());

        }

    }

    public boolean appendData(T obj) throws IOException, ClassNotFoundException {

        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object list = ois.readObject();
        ArrayList data = (ArrayList) list;
        data.add(obj);
        if (count < MAX_BUF_FILE_SIZE) {
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            count++;
            oos.close();
            fos.close();
            return true;
        } else {

            return false;
        }
    }

    public Object getData() throws IOException, ClassNotFoundException {
        if (count != 0) {
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis);

            ArrayList<Object> val = (ArrayList<Object>) ois.readObject();
            Object data = val.get(0);
            val.remove(0);
            fis.close();
            //ois.close();
            count--;
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(val);
            count++;
            oos.close();
            fos.close();
            return data;
        }
        else{
            return null;
        }
    }
    public void delete(){
        new File(path).delete();
    }
    public void move(String newPath) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path);
        ObjectInputStream ois = new ObjectInputStream(fis);

        ArrayList<Object> val = (ArrayList<Object>) ois.readObject();
        fis.close();
        BinaryFile.create(newPath, val);
        BinaryFile.delete(path);
    }
}

class DCCD{
    String path;
    public void create(String info, String path) throws IOException {
        String str = "";
        for (int i = 0; i < info.length(); i++) {
            int num = (int)info.charAt(i);
            num = num / 2;
            str += (char)num;
        }
        TextFile.create(path, str);
    }
    public String open() throws IOException {
        String content = TextFile.read(path);
        String str = "";
        for (int i = 0; i < content.length(); i++) {
            int num = (int)content.charAt(i);
            num = num / 2;
            str += (char)num;
        }
        return str;
    }

}

class Thread1 implements Runnable {
    static Semaphore sem = new Semaphore(1);
    Semaphore spec;

    public void task() throws InterruptedException {
        //sem.acquire();
        System.out.println(name + " task");
        Thread.sleep(1000);
        System.out.println(name + " task is over");
        //sem.release();

    }

    public void task_init() throws InterruptedException {
        //sem.acquire();

        System.out.println(name + " task_init");
        Thread.sleep(2000);
        System.out.println(name + " task_init is over");
//sem.release();

    }

    public void task_finalize() throws InterruptedException {
        //sem.acquire();
        System.out.println(name + " task_finalize");
        Thread.sleep(1000);
        System.out.println(name + " task_finalize is over");
        //sem.release();
    }

    String name;

    Thread1(Semaphore spec, String name) {
        this.spec = spec;
        this.name = name;
    }

    @Override
    public void run() {
        try {
            sem.acquire();
            task_init();
            sem.release();
            while (sem.isFair()) {
            }

            //sem.acquire();

            //sem.release();


            if (spec != null) {
                spec.acquire();
                task();
                task_finalize();
                spec.release();
                return;
            }
            //sem.release();
            //sem.acquire();
            task();
            task_finalize();

            //sem.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

public class Main {
    static Directory dir = new Directory();
    static int i = 0;
    static TextFile txt = new TextFile();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        /*while (i < 10) {
            //dir.create("C:\\Users\\тигор\\IdeaProjects\\_1\\src", "dir" + i);
            i++;
        }

        BinaryFile.create("C:\\Users\\тигор\\IdeaProjects\\_1\\dir1\\gh2", i);
        //dir.delete("C:\\Users\\тигор\\IdeaProjects\\_1\\dir0");
        dir.content("C:\\Users\\тигор\\IdeaProjects\\_1");
        dir.move("C:\\Users\\тигор\\IdeaProjects\\_1\\dir1", "C:\\Users\\тигор\\IdeaProjects\\_1\\dir0");
        // int c = (Integer)BinaryFile.read("C:\\Users\\тигор\\IdeaProjects\\_1\\dir0\\gh2");
        //System.out.println(c);
        txt.create("dir0\\file.txt", "гииииии");
        txt.append("file.txt", "нова інфа");
        BinaryFile.move("C:\\Users\\тигор\\IdeaProjects\\_1\\dir0\\dir1\\gh2",
                "C:\\Users\\тигор\\IdeaProjects\\_1\\dir0");
        TextFile.move("dir0\\file.txt", "dir0\\dir1");
        Buffer buff = new Buffer("buffer");
        buff.appendData(i);
        i = 5;
        buff.appendData(i);
        //List<Object> list = buff.getData();
        //int a = (Integer) list.get(0);
        //int b = (Integer) list.get(1);*/
        Semaphore spec2 = new Semaphore(1);
        /*new Thread(new Thread1(spec2, "third")).start();
        new Thread(new Thread1(spec2, "fourth")).start();
        new Thread(new Thread1(spec2, "first")).start();
        new Thread(new Thread1(spec2, "second")).start();
        new Thread(new Thread1(spec2, "fifth")).start();*/
        //ForTest.TestDir("C:\\Users\\тигор\\IdeaProjects\\_1\\dir1");
        Integer i = 9;
        Buffer b = new Buffer("Buff1");
        b.appendData(i);
        b.appendData(13);
        Integer tg = (Integer)b.getData();
        Integer t2 = (Integer)b.getData();
        //ForTest.getFile(tg, 13);
        //Directory.move("C:\\Users\\тигор\\IdeaProjects\\_1\\dir1", "src");
        DCCD dec = new DCCD();
        dec.create("", "efhr");
    }
}



class ForTest {
    public static void TestDir(String path) throws IOException, ClassNotFoundException {
        Integer a = new File(path).listFiles().length;
        Directory.create(path, "new");
        if (new File(path).listFiles().length != a + 1) {
            Assert.fail();
        } else {
            System.out.println("OK");
        }
    }
    public static void getFile(Object o1, Object o2){
        if (o1.equals(o2)) {

            System.out.println("OK");
            return;
        }
        Assert.fail();
    }
}